<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}
	//login de Administrador
	public function loginA()
		{
			$this->load->view("loginA");
		}
		public function iniciarSesion()
		{
			$this->load->model("Administrador");
			$email_adm=$this->input->post("email_adm");
		  $password_adm=$this->input->post("password_adm");
		  $administradorConectado=$this->Administrador->obtenerPorEmailPassword($email_adm,$password_adm);
		  	if($administradorConectado){
					$this->session->set_userdata("conectado",$administradorConectado);
					$this->session->set_flashdata("Bienvenido","Bienvenido al sistema ".$administradorConectado->nombre_adm.":".$administradorConectado->perfil_adm);
	    		redirect("welcome/index");
		    	}else{
		    	redirect("welcome/loginA");
		    	}
					}
					public function cerrarSesion()
					{
						$this->session->sess_destroy();
						redirect("welcome/loginA");
					}

					}
		//login de socia
	///
